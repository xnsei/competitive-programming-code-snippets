int add(int a, int b, int mod){
    int ans = (a%mod + b%mod)%mod;
    if(ans < 0)ans+= mod;
    return ans;
}

int mult(int a, int b, int mod){
    int ans = (1LL * (a%mod) * (b%mod))%mod;
    if(ans < 0)ans+=mod;
    return ans;
}

int power(int a, int b, int mod){
    int ans = 1;
    while(b) { 
        if(b%2 == 1){
            ans = mult(ans, a, mod);
        }
        a = mult(a, a, mod);
        b/=2;
    }
    return ans;
}

class StringHash{
public:
    int mod; int base;
    string s;
    string r;
    vector<int> hash;
    vector<int> rev_hash;
    vector<int> pow;
    vector<int> inv;

    StringHash(string _s, int _mod, int _base){
        s = _s;
        r = s;
        reverse(r.begin(), r.end());
        mod = _mod;
        base = _base;
        preCalc();
        build();
    }

    void preCalc(){
        int n = s.length();
        int p = 1;
        for(int i = 0; i < n; i++){
            pow.push_back(p);
            p = mult(p, base, mod);
        }

        int invPow = power(base, mod-2, mod);
        p = 1;
        for(int i = 0; i < n; i++){
            inv.push_back(p);
            p = mult(p, invPow, mod);
        }
    }

    void build(){
        int n = s.length();
        if(n == 0)return;
        hash.push_back(s[0] - 'a' + 1);
        for(int i = 1; i < n; i++){
            hash.push_back(add(hash[i-1], mult(s[i] - 'a' + 1, pow[i], mod), mod));
        } 

        rev_hash.push_back(r[0] - 'a' + 1);
        for(int i = 1; i < n; i++){
            rev_hash.push_back(add(rev_hash[i-1], mult(r[i] - 'a' + 1, pow[i], mod), mod));
        } 
    }

    int getHash(int x, int y) {
        int ans = add(hash[y], (x == 0) ? 0 : -hash[x-1], mod);
        ans = mult(ans, inv[x], mod);
        return ans;
    }

    int getReverseHash(int x, int y) {
        int ans = add(rev_hash[y], (x == 0) ? 0 : -rev_hash[x-1], mod);
        ans = mult(ans, inv[x], mod);
        return ans;
    }

    bool isPalindrome(int x, int y){
        int n = s.length();
        int l = n-1 - y;
        int h = n-1 - x;
        return getHash(x,y)==getReverseHash(l,h);
    }

};
